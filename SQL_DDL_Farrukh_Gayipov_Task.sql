--Creating a db
CREATE DATABASE auction_db;

--Creating a new schema
CREATE SCHEMA auction_schema;

--Create Sellers table
CREATE TABLE auction_schema.Sellers (
    seller_id SERIAL PRIMARY KEY,
    seller_name VARCHAR NOT NULL,
    record_ts DATE
);

--Create Buyers table
CREATE TABLE auction_schema.Buyers (
    buyer_id SERIAL PRIMARY KEY,
    buyer_name VARCHAR NOT NULL,
    record_ts DATE
);

-- Create Items table
CREATE TABLE auction_schema.Items (
    item_id SERIAL PRIMARY KEY,
    seller_id INT REFERENCES auction_schema.Sellers(seller_id),
    item_description VARCHAR NOT NULL,
    starting_price DECIMAL NOT NULL,
    record_ts DATE
);

-- Create Auctions table
CREATE TABLE auction_schema.Auctions (
    auction_id SERIAL PRIMARY KEY,
    auction_date DATE NOT NULL CHECK (auction_date > '2000-01-01'),
    auction_place VARCHAR NOT NULL,
    auction_time TIME NOT NULL,
    record_ts DATE
);

-- Create Lots table
CREATE TABLE auction_schema.Lots (
    lot_number SERIAL PRIMARY KEY,
    item_id INT REFERENCES auction_schema.Items(item_id),
    auction_id INT REFERENCES auction_schema.Auctions(auction_id),
    record_ts DATE
);

-- Create Transactions table
CREATE TABLE auction_schema.Transactions (
    transaction_id SERIAL PRIMARY KEY,
    item_id INT REFERENCES auction_schema.Items(item_id),
    buyer_id INT REFERENCES auction_schema.Buyers(buyer_id),
    actual_price DECIMAL CHECK (actual_price >= 0),
    record_ts DATE
);

-- Create Sellers_Items table (Many-to-Many Relationship)
CREATE TABLE auction_schema.Sellers_Items (
    seller_id INT REFERENCES auction_schema.Sellers(seller_id),
    item_id INT REFERENCES auction_schema.Items(item_id),
    record_ts DATE
);

-- Create Buyers_Items table (Many-to-Many Relationship)
CREATE TABLE auction_schema.Buyers_Items (
    buyer_id INT REFERENCES auction_schema.Buyers(buyer_id),
    item_id INT REFERENCES auction_schema.Items(item_id),
    record_ts DATE
);

-- Create Categories table
CREATE TABLE auction_schema.Categories (
    category_id SERIAL PRIMARY KEY,
    category_name VARCHAR NOT NULL UNIQUE,
    record_ts DATE
);

-- Create Item_Categories table (Many-to-Many Relationship)
CREATE TABLE auction_schema.Item_Categories (
    item_id INT REFERENCES auction_schema.Items(item_id),
    category_id INT REFERENCES auction_schema.Categories(category_id),
    record_ts DATE
);

--Adding rows into the db (all record_ts will be null but will be then altered)
INSERT INTO auction_schema.Sellers (seller_name) 
VALUES ('John Doe'), ('Jane Smith');

INSERT INTO auction_schema.Buyers (buyer_name)
VALUES ('Alice Johnson'), ('Bob Wilson');

INSERT INTO auction_schema.Items (seller_id, item_description, starting_price) VALUES
(1, 'Antique Vase', 100.00),
(2, 'Vintage Painting', 200.00);

INSERT INTO auction_schema.Auctions (auction_date, auction_place, auction_time) VALUES
('2023-10-30', 'Art Gallery', '18:00:00'),
('2023-11-05', 'Historical Museum', '14:30:00');

INSERT INTO auction_schema.Lots (item_id, auction_id) VALUES
(1, 1),
(2, 2);

INSERT INTO auction_schema.Transactions (item_id, buyer_id, actual_price) VALUES
(1, 1, 120.00),
(2, 2, 250.00);

INSERT INTO auction_schema.Sellers_Items (seller_id, item_id) VALUES
(1, 1),
(2, 2);

INSERT INTO auction_schema.Buyers_Items (buyer_id, item_id) VALUES
(1, 1),
(2, 2);

INSERT INTO auction_schema.Categories (category_name) VALUES
('Antiques'),
('Paintings');

INSERT INTO auction_schema.Item_Categories (item_id, category_id) VALUES
(1, 1),
(2, 2);

select * from auction_schema.Auctions;

--Altering the record_ts to existing rows
ALTER TABLE auction_schema.Sellers ALTER COLUMN record_ts SET DEFAULT current_date;
ALTER TABLE auction_schema.Buyers ALTER COLUMN record_ts SET DEFAULT current_date;
ALTER TABLE auction_schema.Items ALTER COLUMN record_ts SET DEFAULT current_date;
ALTER TABLE auction_schema.Auctions ALTER COLUMN record_ts SET DEFAULT current_date;
ALTER TABLE auction_schema.Lots ALTER COLUMN record_ts SET DEFAULT current_date;
ALTER TABLE auction_schema.Transactions ALTER COLUMN record_ts SET DEFAULT current_date;
ALTER TABLE auction_schema.Sellers_Items ALTER COLUMN record_ts SET DEFAULT current_date;
ALTER TABLE auction_schema.Buyers_Items ALTER COLUMN record_ts SET DEFAULT current_date;
ALTER TABLE auction_schema.Categories ALTER COLUMN record_ts SET DEFAULT current_date;
ALTER TABLE auction_schema.Item_Categories ALTER COLUMN record_ts SET DEFAULT current_date;

--updating already created rows
UPDATE auction_schema.Sellers SET record_ts = current_date WHERE record_ts IS NULL;
UPDATE auction_schema.Buyers SET record_ts = current_date WHERE record_ts IS NULL;
UPDATE auction_schema.Items SET record_ts = current_date WHERE record_ts IS NULL;;
UPDATE auction_schema.Auctions SET record_ts = current_date WHERE record_ts IS NULL;
UPDATE auction_schema.Lots SET record_ts = current_date WHERE record_ts IS NULL;
UPDATE auction_schema.Transactions SET record_ts = current_date WHERE record_ts IS NULL;
UPDATE auction_schema.Sellers_Items SET record_ts = current_date WHERE record_ts IS NULL;
UPDATE auction_schema.Buyers_Items SET record_ts = current_date WHERE record_ts IS NULL;
UPDATE auction_schema.Categories SET record_ts = current_date WHERE record_ts IS NULL;
UPDATE auction_schema.Item_Categories SET record_ts = current_date WHERE record_ts IS NULL;

